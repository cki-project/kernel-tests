On some larger systems, memory has non-uniform access times. It is organized
in so-called NUMA-nodes. A NUMA node is a part of memory which is accessible
faster for some particular subset of processors.

# lscpu
Architecture:          x86_64
CPU op-mode(s):        32-bit, 64-bit
Byte Order:            Little Endian
CPU(s):                24
On-line CPU(s) list:   0-23
Thread(s) per core:    2
Core(s) per socket:    6
Socket(s):             2
NUMA node(s):          2
Vendor ID:             GenuineIntel
CPU family:            6
Model:                 45
Stepping:              7
CPU MHz:               1200.000
BogoMIPS:              4588.34
Virtualization:        VT-x
L1d cache:             32K
L1i cache:             32K
L2 cache:              256K
L3 cache:              15360K
NUMA node0 CPU(s):     0-5,12-17
NUMA node1 CPU(s):     6-11,18-23

This machine has two NUMA nodes and 24 processors (they are called CPUs, but
that is not very precise). Half of them belongs to NUMA node0 and the second
half to node1.

When a processor accesses data that lies in its NUMA node, it is faster than
accessing data from some other node.  The local access is called LMA and the
remote one is called RMA.

The NUMATOP tool watches what happens around the NUMA nodes and it sees both
LMAs and RMAs. If the machine has only one NUMA node, only LMAs can be seen.

The test tries to trigger LMAs and check whether NUMATOP registers it. If we
have more than one NUMA node, we test RMAs as well.

